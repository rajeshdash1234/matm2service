package com.linkmatm.service.Utils;


import android.content.Context;
import android.content.SharedPreferences;

import com.linkmatm.service.Constants;


public class Session {
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Context ctx;




    public String getUserToken() {
        return prefs.getString(Constants.USER_TOKEN_KEY, null);
    }

    public void setUserToken(String userToken) {
        editor.putString(Constants.USER_TOKEN_KEY, userToken);
        editor.commit();
    }

    public String getFreshnessFactor() {
        return prefs.getString(Constants.NEXT_FRESHNESS_FACTOR, null);
    }

    public void setFreshnessFactor(String freshnessFactor) {
        editor.putString(Constants.NEXT_FRESHNESS_FACTOR, freshnessFactor);
        editor.commit();
    }



    public void clear(){
        prefs = ctx.getSharedPreferences(Constants.EASY_AEPS_PREF_KEY, Context.MODE_PRIVATE);
        editor = prefs.edit();
        editor.clear().commit();
    }


    public Session(Context ctx) {
        this.ctx = ctx;
        prefs = ctx.getSharedPreferences(Constants.EASY_AEPS_PREF_KEY, Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    public void setLoggedIn(boolean logggedIn) {
        editor.putBoolean(Constants.EASY_AEPS_USER_LOGGED_IN_KEY, logggedIn);
        editor.commit();
    }

    public boolean isLoggedIn() {
        return prefs.getBoolean(Constants.EASY_AEPS_USER_LOGGED_IN_KEY, false);
    }
    public void setUsername(String username) {
        editor.putString (Constants.EASY_AEPS_USER_NAME_KEY, username);
        editor.commit();
    }

    public String getUserName() {
        return prefs.getString (Constants.EASY_AEPS_USER_NAME_KEY, null);
    }
    public void setEncryptedString(String encryptedString) {
        editor.putString (Constants.encryptedString, encryptedString);
        editor.commit();
    }


    public String getEncryptedString() {
        return prefs.getString(Constants.encryptedString, null);
    }

    public void setDeviceName(String deviceName) {
        editor.putString (Constants.devicename, deviceName);
        editor.commit();
    }
    public String getDeviceName() {
        return prefs.getString(Constants.devicename, null);
    }

    public void setDeviceMac(String deviceMac) {
        editor.putString (Constants.devicemac, deviceMac);
        editor.commit();
    }
    public String getDeviceMac() {
        return prefs.getString(Constants.devicemac, null);
    }
}